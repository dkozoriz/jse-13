package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.repository.ITaskRepository;
import ru.t1.dkozoriz.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    public List<Task> findAllByProjectId(final String ProjectId) {
        List<Task> projectTasks = new ArrayList<>();
        for (final Task task: tasks) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(ProjectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

    public Task findOneById(final String id) {
        for (final Task task: tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    };

    public int getSize() {
        return tasks.size();
    }

    public void remove(final Task task) {
        if (task == null) return;
        tasks.remove(task);
    }

    public Task removeById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    public Task removeByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

}