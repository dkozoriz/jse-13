package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    List<Task> findAllByProjectId(String ProjectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    int getSize();

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}