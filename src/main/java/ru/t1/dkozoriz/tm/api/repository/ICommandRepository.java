package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}