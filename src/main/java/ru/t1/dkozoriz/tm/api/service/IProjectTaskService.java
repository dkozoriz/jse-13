package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    Task unbindTaskToProject(String projectId, String taskId);

}
