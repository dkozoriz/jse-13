package ru.t1.dkozoriz.tm.service;

import ru.t1.dkozoriz.tm.api.repository.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.ITaskService;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) return null;
        if (index >= getSize()) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        return taskRepository.add(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAllByProjectId(final String ProjectId) {
        if (ProjectId == null || ProjectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(ProjectId);
    }

    public Task findOneById (final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOneById(id);
    }

    public Task findOneByIndex (final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findOneByIndex(index);
    }

    public int getSize () {
        return taskRepository.getSize();
    }

    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}